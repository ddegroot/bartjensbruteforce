#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;



/*

Idee: kies een startvakje, en begin vanaf daar met "lopen"
Het doel is dan om terug te komen bij het startvakje en alle vakjes langs geweest te zijn
(en te voldoen aan de aanwijzingen)
Hierdoor krijg je veel te veel opties om allemaal langs te gaan (je hebt vaak 2 of 3 opties, en dat 11 * 12 = 132 keer)
dus ik hoopte dat je veel posities snel al kon uitsluiten (bijvoorbeeld omdat sommige vakjes niet meer bereikbaar zijn)
Dat viel tegen, het is veel te langzaam
Het probleem is vooral dat je posities krijgt waar sowieso een probleem gaat optreden, maar omdat de route niet direct langs het problematische stuk gaat zie je dat veel later pas
dus ja tijd voor poging 2


*/




int width, height;
int groupAmount;
int group[100][100];
int groupValue[100];
vector<ii> positionsInGroup[100];


bool filled[100][100];
int longestPathInGroup[100];
vector<int> pathLengths[100];

int emptySquares;
int emptyInGroup[100];

ii startPos;
int startGroup;
stack<ii> curPath;

void updateLongestPath(int groupNum) {
	int value = 0;
	for(int pathLength : pathLengths[groupNum]) {
		value = max(value, pathLength);
	}
	longestPathInGroup[groupNum] = value;
}

void add(int x, int y) {
	int prevGroup = group[curPath.top().first][curPath.top().second];
	int newGroup = group[x][y];

	if(prevGroup == newGroup) {
		++pathLengths[newGroup].back();
	} else {
		pathLengths[newGroup].push_back(1);
	}
	updateLongestPath(newGroup);

	filled[x][y] = true;
	curPath.push({x, y});
	--emptySquares;
	--emptyInGroup[newGroup];
}

void removeLast() {
	auto[lastX, lastY] = curPath.top();

	int lastGroup = group[lastX][lastY];
	--pathLengths[lastGroup].back();
	if(pathLengths[lastGroup].back() == 0) {
		pathLengths[lastGroup].pop_back();
	}
	updateLongestPath(lastGroup);


	curPath.pop();
	filled[lastX][lastY] = false;
	++emptySquares;
	++emptyInGroup[lastGroup];
}

bool validPos(int x, int y) {
	return x >= 0 && x < width && y >= 0 && y < height;
}

vector<ii> getNeighbours(int x, int y) {
	vector<ii> result;
	vector<ii> options = {{x + 1, y}, {x - 1, y}, {x, y + 1}, {x, y - 1}};
	for(ii option : options) {

		if(validPos(option.first, option.second)) {
			result.push_back(option);
		}
	}
	return result;
}

set<ii> floodfill(int startX, int startY) {
	set<ii> result;
	stack<ii> frontier;

	frontier.push({startX, startY});
	while(frontier.size() > 0) {
		if(result.count(frontier.top())) {
			frontier.pop();
			continue;
		}
		result.insert(frontier.top());

		auto[x, y] = frontier.top();
		frontier.pop();
		for(ii neighbour : getNeighbours(x, y)) {
			if(filled[neighbour.first][neighbour.second]) {
				continue;
			}
			frontier.push(neighbour);
		}
	}

	return result;
}


set<ii> biggestEmptyComponent(int groupNum) {
	vector<ii> positions = positionsInGroup[groupNum];
	int index = 0;
	set<ii> seen;
	set<ii> result;

	for(int index = 0; index < positions.size(); ++index) {
		if(seen.count(positions[index]) > 0) {
			continue;
		}
		auto[startX, startY] = positions[index];
		if(filled[startX][startY]) {
			seen.insert({startX, startY});
			continue;
		}

		set<ii> component;
		stack<ii> frontier;

		frontier.push({startX, startY});
		while(frontier.size() > 0) {
			if(component.count(frontier.top())) {
				frontier.pop();
				continue;
			}
			component.insert(frontier.top());
			seen.insert(frontier.top());

			auto[x, y] = frontier.top();
			frontier.pop();
			for(ii neighbour : getNeighbours(x, y)) {
				if(filled[neighbour.first][neighbour.second]) {
					continue;
				}
				if(group[neighbour.first][neighbour.second] != groupNum) {
					continue;
				}
				frontier.push(neighbour);
			}
		}

		if(component.size() > result.size()) {
			result = component;
		}
	}

	return result;
	
}

bool deadEnd(int x, int y) {
	if(filled[x][y]) {
		return false;
	}
	int emptyNeighbours = 0;
	for(ii neighbour : getNeighbours(x, y)) {
		if(neighbour == startPos) {
			return false;
		}
		if(neighbour == curPath.top()) {
			return false;
		}
		if(filled[neighbour.first][neighbour.second]) {
			continue;
		}
		++emptyNeighbours;
	}
	return emptyNeighbours == 1;
}

bool articulationTest(int x, int y, int nextX, int nextY) {
	filled[x][y] = true;
	set<ii> reachable = floodfill(nextX, nextY);
	filled[x][y] = false;
	if(reachable.size() + 1 == emptySquares) {
		return true;
	}
	for(ii startNeighbour : getNeighbours(startPos.first, startPos.second)) {
		if(filled[startNeighbour.first][startNeighbour.second]) {
			continue;
		}
		if(reachable.count(startNeighbour) == 0) {
			return true;
		}
	}
	return false;


}


void display() {
	for(int y = 0; y < height; ++y) {
		for(int x = 0; x < width; ++x) {
			char c = 'a' + group[x][y];
			
			if(startPos.first == x && startPos.second == y) {
				cout << "#";
			} else if(curPath.top().first == x && curPath.top().second == y) {
				cout << "+";
			} else if(filled[x][y]) {
				cout << ".";
			} else {
				cout << c;
			}
			
		}
		cout << endl;
	}
}

int iterations = 0;
int passedChecks = 0;


void search() {
	++iterations;
	
	auto[curX, curY] = curPath.top();
	int curGroup = group[curX][curY];

	if(emptySquares == 0) {
		//if(abs(curX - startPos.first) + abs(curY - startPos.second) == 1) {
		cout << "potential answer" << endl;
		display();
		cout << 0 / 0 << endl;
		return;
	}

	int targetValue = groupValue[curGroup];

	if(targetValue != -1) {
		
		if(longestPathInGroup[curGroup] > targetValue) {
			return;
		}
		if(curGroup != startGroup && longestPathInGroup[curGroup] < targetValue &&
				pathLengths[curGroup].back() + emptyInGroup[curGroup] < targetValue) {
			return;
		}
	}


	ii emptyNeighbour = {-1, -1};
	for(ii neighbour : getNeighbours(curX, curY)) {
		if(filled[neighbour.first][neighbour.second]) {
			continue;
		}
		emptyNeighbour = neighbour;
		break;
	}

	if(emptyNeighbour.first == -1) {
		return;
	}

	if(floodfill(emptyNeighbour.first, emptyNeighbour.second).size() < emptySquares) {
		return;
	}


	++passedChecks;
	if(passedChecks % 1000 == 0) {
		cout << passedChecks << endl;
		cout << "iterations: " << iterations << endl;
		cout << "empty squares: " << emptySquares << endl;
		display();
	}

	bool canLeaveGroup = true;
	if(curGroup != startGroup && targetValue != -1) {
		if(longestPathInGroup[curGroup] < targetValue &&
				biggestEmptyComponent(curGroup).size() < targetValue) {
			canLeaveGroup = false;
		}
	}

	for(ii nextPos : getNeighbours(curX, curY)) {

		if(filled[nextPos.first][nextPos.second]) {
			continue;
		}

		int nextGroup = group[nextPos.first][nextPos.second];
		if(nextGroup != curGroup && !canLeaveGroup) {
			continue;
		}

		bool valid = true;


		for(ii neighbour : getNeighbours(curX, curY)) {
			if(filled[neighbour.first][neighbour.second]) {
				continue;
			}
			if(neighbour == nextPos) {
				continue;
			}
			if(!articulationTest(neighbour.first, neighbour.second,
					nextPos.first, nextPos.second)) {
				valid = false;
				break;
			}
		}

		if(!valid) {
			continue;
		}

		add(nextPos.first, nextPos.second);

		
		for(ii neighbour : getNeighbours(curX, curY)) {
			if(deadEnd(neighbour.first, neighbour.second)) {
				valid = false;
				break;
			}
		}

		if(valid) {
			search();
		}
		removeLast();

	}

}


int main() {
	cin >> width >> height >> groupAmount;

	emptySquares = width * height;

	string line;
	for(int y = 0; y < height; ++y) {
		cin >> line;
		for(int x = 0; x < width; ++x) {
			int groupNum = line[x] - 'a';
			group[x][y] = groupNum;
			++emptyInGroup[groupNum];
			positionsInGroup[groupNum].push_back({x, y});
		}
	}

	for(int i = 0; i < groupAmount; ++i) {
		cin >> groupValue[i];
	}


	//startPos = {5, 6};
	//startPos = {4, 10};
	//startPos = {0, 3};
	startPos = {3, 1};
	startGroup = group[startPos.first][startPos.second];
	curPath.push(startPos);
	filled[startPos.first][startPos.second] = true;
	--emptySquares;
	pathLengths[startGroup].push_back(1);
	updateLongestPath(startGroup);

	search();


}