#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;

#define MAX_SIZE 20
#define MAX_GROUPS 15

/*

oke ik heb de hele structuur van de rechthoek eruit gegooid en alles in een graaf gezet
In plaats van een pad volgen gaat het nu steeds op zoek naar een node die nog niet twee verbindingen heeft
en dan kijken hoeveel mogelijke manieren er zijn om die te verbinden
en dan kies je de node met het kleinste aantal opties
Dat zijn maximaal 2 opties, en vaak 1, dus het loopt allemaal iets minder exponentieel uit de hand
nog steeds duurt het wel een beetje lang voordat er een oplossing gevonden (minuut of zo) is maar ik vind het acceptabel



*/

int width, height;
int nodeAmount;
vector<int> adjacencyList[MAX_SIZE * MAX_SIZE];

int groupAmount;
int group[MAX_SIZE * MAX_SIZE];
int groupValue[MAX_GROUPS];
vector<int> nodesInGroup[MAX_GROUPS];


vector<ii> chosenEdges;
vector<int> chosenAdjacencyList[MAX_SIZE * MAX_SIZE];


void chooseEdge(int a, int b) {
	chosenEdges.push_back({a, b});
	chosenAdjacencyList[a].push_back(b);
	chosenAdjacencyList[b].push_back(a);
}

void removeLastEdge() {
	auto[a, b] = chosenEdges.back();
	chosenEdges.pop_back();
	chosenAdjacencyList[a].pop_back();
	chosenAdjacencyList[b].pop_back();
}

int otherEndpoint(int node, int prev) {
	for(int adjacent : chosenAdjacencyList[node]) {
		if(adjacent == prev) {
			continue;
		}
		return otherEndpoint(adjacent, node);
	}
	return node;
}

int otherEndpoint(int node) {
	return otherEndpoint(node, -1);
}

int pathLengthInGroup(int node, int prev) {
	int result = 1;
	for(int adjacent : chosenAdjacencyList[node]) {
		if(adjacent == prev) {
			continue;
		}
		if(group[adjacent] != group[node]) {
			continue;
		}
		result += pathLengthInGroup(adjacent, node);
	}
	return result;
}

int pathLengthInGroup(int node) {
	return pathLengthInGroup(node, -1);
}

bool hasOpenEndpointInGroup(int node, int prev) {
	if(chosenAdjacencyList[node].size() == 1) {
		return true;
	}
	for(int adjacent : chosenAdjacencyList[node]) {
		if(adjacent == prev) {
			continue;
		}
		if(group[adjacent] != group[node]) {
			continue;
		}
		if(hasOpenEndpointInGroup(adjacent, node)) {
			return true;
		}
	}
	return false;
}

bool hasOpenEndpointInGroup(int node) {
	return hasOpenEndpointInGroup(node, -1);
}

int potentialFloodfill(int startNode) {
	vector<bool> seen(nodeAmount, false);
	int result = 0;
	stack<int> frontier;
	frontier.push(startNode);

	while(frontier.size() > 0) {
		int node = frontier.top();
		frontier.pop();
		if(seen[node]) {
			continue;
		}
		if(chosenAdjacencyList[node].size() == 2) {
			continue;
		}
		seen[node] = true;
		++result;
		if(chosenAdjacencyList[node].size() == 1) {
			seen[otherEndpoint(node)] = true;
			++result;
			for(int adjacent : adjacencyList[otherEndpoint(node)]) {
				frontier.push(adjacent);
			}
			continue;
		}
		for(int adjacent : adjacencyList[node]) {
			frontier.push(adjacent);
		}
	}

	return result;
}

bool finishablePosition() {

	vector<int> notChosen;
	vector<int> halfChosen;

	for(int node = 0; node < nodeAmount; ++node) {
		if(chosenAdjacencyList[node].size() == 0) {
			notChosen.push_back(node);
		} else if(chosenAdjacencyList[node].size() == 1) {
			halfChosen.push_back(node);
		}
	}

	if(notChosen.size() > 0) {
		if(potentialFloodfill(notChosen.back()) < notChosen.size() + halfChosen.size()) {
			
			return false;
		}
	}

	for(int groupNum = 0; groupNum < groupAmount; ++groupNum) {
		int targetValue = groupValue[groupNum];

		if(targetValue == -1) {
			continue;
		}

		int openNodes = nodesInGroup[groupNum].size();
		int longestPath = 0;
		for(int node : nodesInGroup[groupNum]) {
			longestPath = max(longestPath, pathLengthInGroup(node));

			if(chosenAdjacencyList[node].size() == 2 && !hasOpenEndpointInGroup(node)) {
				--openNodes;
			}
		}

		if(longestPath > targetValue) {
			return false;
		}

		if(longestPath < targetValue && openNodes < targetValue) {
			return false;
		}
	}

	return true;
}

int getId(int x, int y) {
	return y * width + x;
}

void display() {
	for(int y = 0; y < height; ++y) {
		for(int x = 0; x < width; ++x) {
			int node = getId(x, y);
			string c;
			bool goesRight = false;
			bool goesLeft = false;
			bool goesDown = false;
			bool goesUp = false;
			for(int adjacent : chosenAdjacencyList[node]) {
				if(adjacent == getId(x + 1, y)) {
					goesRight = true;
				} else if(adjacent == getId(x - 1, y)) {
					goesLeft = true;
				} else if(adjacent == getId(x, y + 1)) {
					goesDown = true;
				} else if(adjacent == getId(x, y - 1)) {
					goesUp = true;
				}
			}

			if(chosenAdjacencyList[node].size() == 0) {
				c = ".";
			} else if(chosenAdjacencyList[node].size() == 1) {
				c = "+";
			} else {
				if(goesRight && goesLeft) {
					c = "━";
				} else if(goesRight && goesDown) {
					c = "┎";
				} else if(goesRight && goesUp) {
					c = "┗";
				} else if(goesLeft && goesDown) {
					c = "┑";
				} else if(goesLeft && goesUp) {
					c = "┚";
				} else {
					c = "│";
				}
			}

			cout << c;

		}

		cout << endl;
	}
}


int iterations = 0;

void search() {

	++iterations;
	if(iterations % 100 == 0) {
		cerr << "iterations: " << iterations << endl;
	}

	if(chosenEdges.size() == nodeAmount) {
		display();
		return;
	}

	vector<vector<ii>> shortestOptionsList(10);

	for(int node = 0; node < nodeAmount; ++node) {
		vector<vector<ii>> potentialOptions;
		if(chosenAdjacencyList[node].size() == 0) {
			for(int firstAdjacent : adjacencyList[node]) {
				if(chosenAdjacencyList[firstAdjacent].size() == 2) {
					continue;
				}
				for(int secondAdjacent : adjacencyList[node]) {
					if(firstAdjacent >= secondAdjacent) {
						continue;
					}
					if(chosenAdjacencyList[secondAdjacent].size() == 2) {
						continue;
					}

					if(chosenEdges.size() < nodeAmount - 2 &&
							chosenAdjacencyList[firstAdjacent].size() == 1 &&
							chosenAdjacencyList[secondAdjacent].size() == 1 &&
							otherEndpoint(firstAdjacent) == secondAdjacent) {
						continue;
					}

					potentialOptions.push_back({{node, firstAdjacent}, {node, secondAdjacent}});


				}
			}

		} else if(chosenAdjacencyList[node].size() == 1) {
			for(int adjacent : adjacencyList[node]) {
				if(adjacent == chosenAdjacencyList[node].back()) {
					continue;
				}
				if(chosenAdjacencyList[adjacent].size() == 2) {
					continue;
				}
				if(chosenEdges.size() < nodeAmount - 1 &&
						chosenAdjacencyList[adjacent].size() == 1 &&
						otherEndpoint(adjacent) == node) {
					continue;
				}
				potentialOptions.push_back({{node, adjacent}});
			}
		} else {
			continue;
		}

		vector<vector<ii>> actualOptions;
		for(vector<ii> edgeList : potentialOptions) {
			for(ii edge : edgeList) {
				chooseEdge(edge.first, edge.second);
			}
			if(finishablePosition()) {
				actualOptions.push_back(edgeList);
			}
			for(ii edge : edgeList) {
				removeLastEdge();
			}

			if(actualOptions.size() >= shortestOptionsList.size()) {
				break;
			}
		}

		
		if(actualOptions.size() == 0) {
			return;
		}

		if(actualOptions.size() < shortestOptionsList.size()) {
			shortestOptionsList = actualOptions;
		}

	}

	for(vector<ii> edgeList : shortestOptionsList) {
		for(ii edge : edgeList) {
			chooseEdge(edge.first, edge.second);
		}
		search();
		for(ii edge : edgeList) {
			removeLastEdge();
		}
	}
}



int main() {
	cin >> width >> height >> groupAmount;

	nodeAmount = width * height;

	string line;
	for(int y = 0; y < height; ++y) {
		cin >> line;
		for(int x = 0; x < width; ++x) {
			int id = getId(x, y);
			int groupNum = line[x] - 'a';
			group[id] = groupNum;
			nodesInGroup[groupNum].push_back(id);

			if(x > 0) {
				adjacencyList[id].push_back(getId(x - 1, y));
			}
			if(x < width - 1) {
				adjacencyList[id].push_back(getId(x + 1, y));
			}
			if(y > 0) {
				adjacencyList[id].push_back(getId(x, y - 1));
			}
			if(y < height - 1) {
				adjacencyList[id].push_back(getId(x, y + 1));
			}
		}
	}

	for(int i = 0; i < groupAmount; ++i) {
		cin >> groupValue[i];
	}

	search();
}