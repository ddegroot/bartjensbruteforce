#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;


/*

Lijkt best wel op versie 1
Deze keer ga je de route niet alleen naar voren uitbreiden, maar ook naar achteren
Het idee was dat je daardoor eerder bepaalde problematische dingen tegenkomt
Dat viel tegen, het is weer veel te langzaam
Met eigenlijk hetzelfde probleem als de eerste poging
(deze code is wel iets mooier)


*/


int width, height;
int groupAmount;
int group[100][100];
int groupValue[100];
vector<ii> positionsInGroup[100];


bool filled[100][100];
int longestPathInGroup[100];
deque<int> pathLengths[100];

int emptySquares;
int emptyInGroup[100];

deque<ii> curPath;

int frontAmount = 0;
int backAmount = 0;

void updateLongestPath(int groupNum) {
	int value = 0;
	for(int pathLength : pathLengths[groupNum]) {
		value = max(value, pathLength);
	}
	longestPathInGroup[groupNum] = value;
}

void addFront(int x, int y) {
	int prevGroup = group[curPath.front().first][curPath.front().second];
	int newGroup = group[x][y];

	if(prevGroup == newGroup) {
		++pathLengths[newGroup].front();
	} else {
		pathLengths[newGroup].push_front(1);
	}
	updateLongestPath(newGroup);

	filled[x][y] = true;
	curPath.push_front({x, y});
	--emptySquares;
	--emptyInGroup[newGroup];
	++frontAmount;
}

void addBack(int x, int y) {
	int prevGroup = group[curPath.back().first][curPath.back().second];
	int newGroup = group[x][y];

	if(prevGroup == newGroup) {
		++pathLengths[newGroup].back();
	} else {
		pathLengths[newGroup].push_back(1);
	}
	updateLongestPath(newGroup);

	filled[x][y] = true;
	curPath.push_back({x, y});
	--emptySquares;
	--emptyInGroup[newGroup];
	++backAmount;
}


void removeFront() {
	auto[lastX, lastY] = curPath.front();

	int lastGroup = group[lastX][lastY];
	--pathLengths[lastGroup].front();
	if(pathLengths[lastGroup].front() == 0) {
		pathLengths[lastGroup].pop_front();
	}
	updateLongestPath(lastGroup);


	curPath.pop_front();
	filled[lastX][lastY] = false;
	++emptySquares;
	++emptyInGroup[lastGroup];
	--frontAmount;
}

void removeBack() {
	auto[lastX, lastY] = curPath.back();

	int lastGroup = group[lastX][lastY];
	--pathLengths[lastGroup].back();
	if(pathLengths[lastGroup].back() == 0) {
		pathLengths[lastGroup].pop_back();
	}
	updateLongestPath(lastGroup);


	curPath.pop_back();
	filled[lastX][lastY] = false;
	++emptySquares;
	++emptyInGroup[lastGroup];
	--backAmount;
}

bool validPos(int x, int y) {
	return x >= 0 && x < width && y >= 0 && y < height;
}

vector<ii> getNeighbours(int x, int y) {
	vector<ii> result;
	vector<ii> options = {{x + 1, y}, {x - 1, y}, {x, y + 1}, {x, y - 1}};
	for(ii option : options) {

		if(validPos(option.first, option.second)) {
			result.push_back(option);
		}
	}
	return result;
}

vector<ii> getEmptyNeighbours(int x, int y) {
	vector<ii> result;
	for(ii option : getNeighbours(x, y)) {

		if(!filled[option.first][option.second]) {
			result.push_back(option);
		}
	}
	return result;
}

set<ii> floodfill(int startX, int startY) {
	set<ii> result;
	stack<ii> frontier;

	frontier.push({startX, startY});
	while(frontier.size() > 0) {
		if(result.count(frontier.top())) {
			frontier.pop();
			continue;
		}
		result.insert(frontier.top());

		auto[x, y] = frontier.top();
		frontier.pop();
		for(ii neighbour : getNeighbours(x, y)) {
			if(filled[neighbour.first][neighbour.second]) {
				continue;
			}
			frontier.push(neighbour);
		}
	}

	return result;
}


set<ii> biggestEmptyComponent(int groupNum) {
	vector<ii> positions = positionsInGroup[groupNum];
	int index = 0;
	set<ii> seen;
	set<ii> result;

	for(int index = 0; index < positions.size(); ++index) {
		if(seen.count(positions[index]) > 0) {
			continue;
		}
		auto[startX, startY] = positions[index];
		if(filled[startX][startY]) {
			seen.insert({startX, startY});
			continue;
		}

		set<ii> component;
		stack<ii> frontier;

		frontier.push({startX, startY});
		while(frontier.size() > 0) {
			if(component.count(frontier.top())) {
				frontier.pop();
				continue;
			}
			component.insert(frontier.top());
			seen.insert(frontier.top());

			auto[x, y] = frontier.top();
			frontier.pop();
			for(ii neighbour : getNeighbours(x, y)) {
				if(filled[neighbour.first][neighbour.second]) {
					continue;
				}
				if(group[neighbour.first][neighbour.second] != groupNum) {
					continue;
				}
				frontier.push(neighbour);
			}
		}

		if(component.size() > result.size()) {
			result = component;
		}
	}

	return result;
	
}

bool deadEnd(int x, int y) {
	int emptyNeighbours = 0;
	for(ii neighbour : getNeighbours(x, y)) {
		if(neighbour == curPath.front()) {
			return false;
		}
		if(neighbour == curPath.back()) {
			return false;
		}
		if(filled[neighbour.first][neighbour.second]) {
			continue;
		}
		++emptyNeighbours;
	}
	return emptyNeighbours == 1;
}

bool articulationTest(int x, int y) {
	ii emptyNeighbour = {-1, -1};
	for(ii neighbour : getNeighbours(x, y)) {
		if(filled[neighbour.first][neighbour.second]) {
			continue;
		}
		emptyNeighbour = neighbour;
		break;
	}

	if(emptyNeighbour.first == -1) {
		throw invalid_argument("probably shouldn't happen");
	}

	filled[x][y] = true;
	set<ii> firstComponent = floodfill(emptyNeighbour.first, emptyNeighbour.second);
	filled[x][y] = false;
	if(firstComponent.size() + 1 == emptySquares) {
		return true;
	}
	bool firstHasAdjacent = false;
	bool secondHasAdjacent = false;
	for(ii frontNeighbour : getNeighbours(curPath.front().first, curPath.front().second)) {
		if(filled[frontNeighbour.first][frontNeighbour.second]) {
			continue;
		}
		if(firstComponent.count(frontNeighbour) > 0) {
			firstHasAdjacent = true;
		} else {
			secondHasAdjacent = true;
		}
	}
	for(ii backNeighbour : getNeighbours(curPath.back().first, curPath.back().second)) {
		if(filled[backNeighbour.first][backNeighbour.second]) {
			continue;
		}
		if(firstComponent.count(backNeighbour) > 0) {
			firstHasAdjacent = true;
		} else {
			secondHasAdjacent = true;
		}
	}
	return firstHasAdjacent && secondHasAdjacent;

}


void display() {
	for(int y = 0; y < height; ++y) {
		for(int x = 0; x < width; ++x) {
			char c = 'a' + group[x][y];
			
			if(curPath.front().first == x && curPath.front().second == y) {
				cout << "-";
			} else if(curPath.back().first == x && curPath.back().second == y) {
				cout << "+";
			} else if(filled[x][y]) {
				cout << ".";
			} else {
				cout << c;
			}
			
		}
		cout << endl;
	}
}



bool finishableState(vector<int> groupsToCheck, vector<ii> articulationsToCheck) {
	auto[frontX, frontY] = curPath.front();
	int frontGroup = group[frontX][frontY];
	int frontTarget = groupValue[frontGroup];
	int frontPathLength = pathLengths[frontGroup].front();
	vector<ii> frontEmptyNeighbours = getEmptyNeighbours(frontX, frontY);

	auto[backX, backY] = curPath.back();
	int backGroup = group[backX][backY];
	int backTarget = groupValue[backGroup];
	int backPathLengths = pathLengths[backGroup].back();
	vector<ii> backEmptyNeighbours = getEmptyNeighbours(backX, backY);


	if(frontEmptyNeighbours.size() == 0 || backEmptyNeighbours.size() == 0) {
		return false;
	}

	ii emptySquare = frontEmptyNeighbours.back();

	if(floodfill(emptySquare.first, emptySquare.second).size() < emptySquares) {
		return false;
	}

	if(frontTarget != -1 && frontPathLength > frontTarget) {
		return false;
	}
	if(backTarget != -1 && backPathLengths > backTarget) {
		return false;
	}

	for(int groupNum : groupsToCheck) {
		if(groupNum == frontGroup || groupNum == backGroup) {
			continue;
		}

		if(longestPathInGroup[groupNum] < groupValue[groupNum] &&
				biggestEmptyComponent(groupNum).size() < groupValue[groupNum]) {
			return false;
		}
	}

	for(ii square : articulationsToCheck) {
		if(filled[square.first][square.second]) {
			continue;
		}
		if(!articulationTest(square.first, square.second)) {
			return false;
		}
	}

	return true;


}


int iterations = 0;

void search() {
	++iterations;

	if(emptySquares < 5) {
		//if(abs(curX - startPos.first) + abs(curY - startPos.second) == 1) {
		cout << "potential answer" << endl;
		display();
		throw invalid_argument("we done?");
		return;
	}

	auto[frontX, frontY] = curPath.front();
	int frontGroup = group[frontX][frontY];
	vector<ii> frontEmptyNeighbours = getEmptyNeighbours(frontX, frontY);

	auto[backX, backY] = curPath.back();
	int backGroup = group[backX][backY];
	vector<ii> backEmptyNeighbours = getEmptyNeighbours(backX, backY);

	if(iterations % 1000 == 0) {
		cout << "iterations: " << iterations << endl;
		cout << "empty squares: " << emptySquares << endl;
		display();
	}

	vector<ii> frontOptions;
	vector<ii> backOptions;

	for(ii frontEmptyNeighbour : frontEmptyNeighbours) {
		addFront(frontEmptyNeighbour.first, frontEmptyNeighbour.second);
		if(finishableState({frontGroup}, frontEmptyNeighbours)) {
			frontOptions.push_back(frontEmptyNeighbour);
		}
		removeFront();
	}

	for(ii backEmptyNeighbour : backEmptyNeighbours) {
		addBack(backEmptyNeighbour.first, backEmptyNeighbour.second);
		if(finishableState({backGroup}, backEmptyNeighbours)) {
			backOptions.push_back(backEmptyNeighbour);
		}
		removeBack();
	}

	if(frontOptions.size() == 0 || backOptions.size() == 0) {
		return;
	}

	/*
	if(frontOptions.size() == 1) {
		addFront(frontOptions.back().first, frontOptions.back().second);
		search();
		removeFront();
		return;
	}

	if(backOptions.size() == 1) {
		addBack(backOptions.back().first, backOptions.back().second);
		search();
		removeBack();
		return;
	}
	*/


	if(frontAmount < backAmount) {
		for(ii frontOption : frontOptions) {
			addFront(frontOption.first, frontOption.second);
			search();
			removeFront();
		}
	} else {
		for(ii backOption : backOptions) {
			addBack(backOption.first, backOption.second);
			search();
			removeBack();
		}
	}

}


int main() {
	cin >> width >> height >> groupAmount;

	emptySquares = width * height;

	string line;
	for(int y = 0; y < height; ++y) {
		cin >> line;
		for(int x = 0; x < width; ++x) {
			int groupNum = line[x] - 'a';
			group[x][y] = groupNum;
			++emptyInGroup[groupNum];
			positionsInGroup[groupNum].push_back({x, y});
		}
	}

	for(int i = 0; i < groupAmount; ++i) {
		cin >> groupValue[i];
	}


	ii startPos = {5, 6};
	//ii startPos = {4, 10};
	//ii startPos = {0, 3};
	//ii startPos = {0, 0};
	int startGroup = group[startPos.first][startPos.second];
	curPath.push_back(startPos);
	filled[startPos.first][startPos.second] = true;
	--emptySquares;
	pathLengths[startGroup].push_back(1);
	updateLongestPath(startGroup);

	search();
}