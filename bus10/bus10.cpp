#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;

#define MAXN 10

/*

deze is zo slecht hij doet er echt een kwartier over
er zit ook ergens een fout in het programma maar ik had geen zin om die te zoeken
en alle debug informatie zit er ook nog in



*/

int n;

vector<vector<int>> groups;


int toNode(int x, int y) {
    return x + y * n;
}


vector<int> adjacent(int i) {

    vector<int> result;
    if(i >= n) {
        result.push_back(i - n);
    }
    if(i < n * n - n) {
        result.push_back(i + n);
    }
    if((i % n) > 0) {
        result.push_back(i - 1);
    }
    if((i % n) < n - 1) {
        result.push_back(i + 1);
    }

    return result;

}

vector<int> surrounding(int i) {
    vector<int> result = adjacent(i);
    if((i % n) > 0) {
        if(i >= n) {
            result.push_back(i - 1 - n);
        }
        if(i < n * n - n) {
            result.push_back(i - 1 + n);
        }
    }
    if((i % n) < n - 1) {
        if(i >= n) {
            result.push_back(i + 1 - n);
        }
        if(i < n * n - n) {
            result.push_back(i + 1 + n);
        }
    }


    return result;
}






vector<pair<int, vector<int>>> hints;


int color[MAXN * MAXN];
bool press[MAXN * MAXN];
stack<ii> actions;


void doAction(ii action) {
    auto[node, value] = action;
    if((value == 2 && press[node]) || (value < 2 && color[node] >= 0)) {
        cerr << "Node already chosen!" << endl;
        cerr << node << " " << value << endl;
        exit(0);
    }

    if(value == 2) {
        press[node] = true;
        if(color[node] == -1) {
            color[node] = 1;
            value = 3;
        }
        if(color[node] == 0) {
            cerr << "Oh oh" << endl;
            exit(0);
        }
    } else {
        color[node] = value;
    }

    actions.push({node, value});
}

void undo() {
    auto[node, value] = actions.top();
    actions.pop();
    if(value >= 2) {
        press[node] = false;
        if(value == 3) {
            color[node] = -1;
        }
    } else {
        color[node] = -1;
    }
}


void display() {
    for(int y = n - 1; y >= 0; --y) {
        for(int x = 0; x < n; ++x) {
            int node = toNode(x, y);
            if(color[node] == 0) {
                cout << 0;
            } else if(color[node] == 1) {
                if(press[node]) {
                    cout << 2;
                } else {
                    cout << 1;
                }
            } else {
                cout << " ";
            }
        }
        cout << endl;
    }
}

bool localValid(int i) {

    if(color[i] <= 0) {
        return true;
    }

    if(press[i]) {
        for(int j : surrounding(i)) {
            if(press[j]) {
                return false;
            }
        }
    }

    int filledAmount = 0;
    vector<int> filled;
    int unchosen = 0;
    for(int j : adjacent(i)) {
        if(color[j] == 1) {
            ++filledAmount;
            filled.push_back(j);
        } else if(color[j] == -1) {
            ++unchosen;
        }
    }

    if(filledAmount > 2 || filledAmount + unchosen < 2) {
        return false;
    }

    if(filledAmount == 2) {
        int between = filled[0] + filled[1] - i;
        if(between != i) {
            if(color[between] == 1) {
                //cout << between << " " << i << " " << filled[0] << " " << filled[1] << endl;
                return false;
            }
            if(color[2 * i - between] == 1) {
                return false;
            }
        }
    }

    return true;
}

bool floodfillValid() {
    bool seen[n * n];
    for(int i = 0; i < n * n; ++i) {
        seen[i] = false;
    }

    int next = 0;

    int unique = 0;
    bool foundClosed = false;


    while(next < n * n) {
        if(seen[next] || color[next] <= 0) {
            ++next;
            continue;
        }
        seen[next] = true;
        ++unique;

        bool closed = true;

        stack<int> s;
        s.push(next);
        while(!s.empty()) {
            int t = s.top();
            int filled = 0;
            s.pop();
            for(int j : adjacent(t)) {
                if(color[j] <= 0) {
                    continue;
                }
                ++filled;
                if(seen[j]) {
                    continue;
                }
                seen[j] = true;
                s.push(j);
            }

            if(filled != 2) {
                closed = false;
            }
        }

        if(closed) {
            foundClosed = true;
        }


        ++next;
    }

    return !foundClosed || unique == 1;
}

bool openEnd(int i) {
    if(color[i] <= 0) {
        return false;
    }

    int filled = 0;
    for(int j : adjacent(i)) {
        if(color[j] == 1) {
            ++filled;
        }
    }

    return filled < 2;
}

bool completable() {
    bool seen[n * n];
    for(int i = 0; i < n * n; ++i) {
        seen[i] = false;
    }

    int openEnds = 0;

    int next = 0;

    while(next < n * n) {
        if(!openEnd(next)) {
            ++next;
            continue;
        }
        ++openEnds;

        if(seen[next]) {
            ++next;
            continue;
        }
        
        int open = -1;

        stack<int> s;

        for(int i : adjacent(next)) {
            if(color[i] <= 0) {
                s.push(i);
            }
        }

        while(!s.empty()) {
            int t = s.top();
            s.pop();
            for(int j : adjacent(t)) {
                if(color[j] == 1) {
                    if(j != next && openEnd(j)) {
                        seen[j] = true;
                        seen[next] = true;
                    }
                    continue;
                }
                if(color[j] == 0) {
                    continue;
                }
                if(seen[j]) {
                    continue;
                }
                seen[j] = true;
                s.push(j);
            }
        }

        ++next;
    }

    if(openEnds > 1) {
        for(int i = 0; i < n * n; ++i) {
            if(openEnd(i) && !seen[i]) {
                return false;
            }
        }
    }

    return true;
}


pair<int, vector<ii>> leastOptions(int depth) {

    const vector<ii> EMPTY;

    //if invalid: return {0, nullptr)

    int missing = 0;


    for(int i = 0; i < n * n; ++i) {
        if(!localValid(i)) {
            return {0, EMPTY};
        }
    }

    

    for(auto[hint, nodes] : hints) {
        int sum = 0;
        int capacity = 0;
        for(int i : nodes) {
            if(color[i] == 1) {
                ++sum;
                if(press[i]) {
                    ++sum;
                } else {
                    ++capacity;
                }
            } else if(color[i] == -1) {
                capacity += 2;
            }
        }
        if(sum > hint || sum + capacity < hint) {
            return {0, EMPTY};
        }

        if(sum != hint) {
            missing += hint - sum;
        }
    }



    for(vector<int> group : groups) {
        bool two = false;
        bool unchosen = false;
        for(int node : group) {
            if(press[node]) {
                if(two) {
                    return {0, EMPTY};
                }
                two = true;
            } else if(color[node] != 0) {
                unchosen = true;
            }
        }
        if(!two && !unchosen) {
            return {0, EMPTY};
        }
        if(!two) {
            ++missing;
        }
    }


    if(!floodfillValid()) {
        return {0, EMPTY};
    }

    if(!completable()) {
        return {0, EMPTY};
    }

    if(missing < 5) {
        cerr << "done? ---------------------------------------" << endl;
        display();
        if(missing == 0) {
            exit(0);
        }
    }


    if(depth == 0) {
        return {1, EMPTY};
    }




    vector<vector<ii>> optionLists;


    for(int i = 0; i < n * n; ++i) {
        if(color[i] <= 0) {
            continue;
        }

        vector<ii> options;
        int filled = 0;
        for(int j : adjacent(i)) {
            if(color[j] == 1) {
                ++filled;
            } else if(color[j] == -1) {
                options.push_back({j, 1});
            }
        }

        if(filled < 2) {
            optionLists.push_back(options);
        }
    }

    for(auto[hint, nodes] : hints) {
        vector<ii> ones;
        vector<ii> all;

        int sum = 0;
        for(int i : nodes) {
            if(color[i] == 1) {
                ++sum;
                if(press[i]) {
                    ++sum;
                } else {
                    all.push_back({i, 2});
                }
            } else if(color[i] == -1) {
                ones.push_back({i, 1});
                all.push_back({i, 1});
            }

            
        }


        if(sum + 2 < hint) {

            optionLists.push_back(ones);
        } else if(sum < hint) {
            optionLists.push_back(all);
        }
    }

    
    for(vector<int> group : groups) {
        bool two = false;
        vector<ii> options;
        for(int i : group) {
            if(press[i]) {
                two = true;
                break;
            }

            if(color[i] != 0) {
                options.push_back({i, 2});
            }
        }

        if(!two) {
            optionLists.push_back(options);
        }
    }

    /*
    for(int i = 0; i < n * n; ++i) {
        if(color[i] == -1) {
            optionLists.push_back({{i, 0}, {i, 1}});
        }
    }
    */


    pair<int, vector<ii>> best = {9999999, vector<ii>()};

    for(vector<ii> optionList : optionLists) {
        int total = 0;
        for(ii option : optionList) {
            doAction(option);
            total += leastOptions(depth - 1).first;
            undo();
        }

        if(total < best.first) {
            best = {total, optionList};
        }
    }

    if(best.first > 1000000) {
        cerr << "?" << endl;
        display();
        exit(0);
    }

    return best;

}

int forks = 0;

int iterations = 0;

void search(long long a, long long b) {

    const long long MAXB = 100000;

    if(b > MAXB) {
        b = MAXB;
    }



    ++iterations;
    if(iterations >= 100000000) {
        exit(0);
    }
    
    
    int amount;
    vector<ii> options;

    if(b < 100) {
        tie(amount, options) = leastOptions(2);
    } else {
        tie(amount, options) = leastOptions(1);
    }

    if(amount == 0) {
        return;
    }

    if(iterations % 1000 == 0) {
        display();
        cout << amount << " options" << endl << endl;
        cout << iterations << " iterations" << endl << endl;
    }

    int optionAmount = options.size();

    for(int i = 0; i < optionAmount; ++i) {
        doAction(options[i]);
        search((a - 1) * optionAmount + (i + 1), b * optionAmount);
        undo();
    }

    if(b < MAXB) {
        cerr << a << " out of " << b << " (" << (100 * a / b) << ")" << endl;
    }

    

    
}





int main() {
    
    cin >> n;

    for(int i = 0; i < n * n; ++i) {
        color[i] = -1;
        press[i] = false;
    }

    for(int i = 0; i < n; ++i) {
        vector<int> group0;
        vector<int> group1;
        for(int j = 0; j < n; ++j) {
            group0.push_back(toNode(i, j));
            group1.push_back(toNode(j, i));
        }

        groups.push_back(group0);
        groups.push_back(group1);
    }


    int hintAmount, hint, x, y;

    cin >> hintAmount;


    for(int i = 0; i < hintAmount; ++i) {
        cin >> hint >> x >> y;
        int node = toNode(x, y);
        color[node] = 0;
        hints.push_back({hint, surrounding(node)});
    }

    search(1, 1);
}