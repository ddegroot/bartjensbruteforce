#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;

#define POSITIONS 36
#define GROUP_AMOUNT 18

/*
oke eigenlijk is deze helemaal niet zo spannend (om te programmeren, de puzzel zelf is erg goed)
Het enige wat ik gedaan heb is een functie die checkt of het een bord legaal is
en dan kijkt het programma gewoon steeds waar de minste opties zijn en het probeert alle opties

Stiekem is het programma te langzaam trouwens
Maar het begint rechtsboven met oplossen, en de eerste gok is een 1, en dat is toevallig goed


*/


int blockNum[POSITIONS];

int groups[GROUP_AMOUNT][6];

vector<ii> lines[4];

int grid[POSITIONS];

bool prime[70];


bool checkDifferentSum(vector<ii> line) {
	int seen = 0;
	for(auto[first, second] : line) {
		if(!grid[first] || !grid[second]) {
			continue;
		}
		int mask = 1 << (grid[first] + grid[second]);
		if(seen & mask) {
			return false;
		}
		seen |= mask;
	}
	return true;
}

bool checkGermanWhispers(vector<ii> line) {
	for(auto[first, second] : line) {
		if(!grid[first] || !grid[second]) {
			continue;
		}
		if(abs(grid[first] - grid[second]) < 3) {
			return false;
		}
	}
	return true;
}

bool checkPrime(vector<ii> line) {
	for(auto[first, second] : line) {
		if(!grid[first] || !grid[second]) {
			continue;
		}

		if(prime[10 * grid[first] + grid[second]]) {
			continue;
		}
		if(prime[10 * grid[second] + grid[first]]) {
			continue;
		}
		return false;
	}
	return true;
}

bool checkRegionSum(vector<ii> line) {
	int blockSum[6];
	for(int i = 0; i < 6; ++i) {
		blockSum[i] = 0;
	}

	set<int> relevantPositions;
	for(auto[first, second] : line) {
		relevantPositions.insert(first);
		relevantPositions.insert(second);
	}

	for(int pos : relevantPositions) {
		if(grid[pos]) {
			blockSum[blockNum[pos]] += grid[pos];
		} else {
			blockSum[blockNum[pos]] -= 100;
		}
	}

	int targetValue = 0;
	for(int i = 0; i < 6; ++i) {
		if(blockSum[i] <= 0) {
			continue;
		}

		if(targetValue == 0) {
			targetValue = blockSum[i];
		} else if(blockSum[i] != targetValue) {
			return false;
		}
	}

	return true;
}

bool checkRule(int rule, int line) {
	if(rule == 0) {
		return checkDifferentSum(lines[line]);
	} else if(rule == 1) {
		return checkGermanWhispers(lines[line]);
	} else if(rule == 2) {
		return checkPrime(lines[line]);
	} else if(rule == 3) {
		return checkRegionSum(lines[line]);
	}

	return false;
}

bool ruleChosen[4];

bool checkRules(int nextLine) {

	if(nextLine >= 4) {
		return true;
	}

	if(nextLine == 0) {
		for(int i = 0; i < 4; ++i) {
			ruleChosen[i] = false;
		}
	}

	for(int rule = 0; rule < 4; ++rule) {
		if(!checkRule(rule, nextLine)) {
			continue;
		}

		if(ruleChosen[rule]) {
			continue;
		}


		ruleChosen[rule] = true;
		if(checkRules(nextLine + 1)) {
			return true;
		}
		ruleChosen[rule] = false;
	}
	return false;
}

bool checkGroups() {
	for(int i = 0; i < GROUP_AMOUNT; ++i) {
		int seen = 0;
		for(int pos : groups[i]) {
			if(!grid[pos]) {
				continue;
			}
			int mask = 1 << grid[pos];
			if(seen & mask) {
				return false;
			}
			seen |= mask;
		}
	}
	return true;
}


bool staticValidCheck() {

	if(!checkGroups()) {
		return false;
	}

	if(!checkRules(0)) {
		return false;
	}

	return true;
}


int toPosition(int x, int y) {
	return 6 * y + x;
}

void display() {
	for(int y = 5; y >= 0; --y) {
		for(int x = 0; x < 6; ++x) {
			cout << grid[toPosition(x, y)];
		}
		cout << endl;
	}
}


void solve() {

	vector<ii> leastOptions;


	for(int group = 0; group < GROUP_AMOUNT; ++group) {
		for(int num = 1; num < 6; ++num) {
			vector<ii> options;
			bool skip = false;

			for(int pos : groups[group]) {
				if(grid[pos] == num) {
					skip = true;
				}
				if(!grid[pos]) {
					options.push_back({pos, num});
				}
			}

			if(skip) {
				continue;
			}

			if(options.size() == 0) {
				return;
			}

			if(leastOptions.size() == 0 || options.size() <= leastOptions.size()) {
				leastOptions = options;
			}
		}
	}

	for(int pos = 0; pos < POSITIONS; ++pos) {
		if(grid[pos]) {
			continue;
		}

		vector<ii> options;
		for(int num = 1; num <= 6; ++num) {
			grid[pos] = num;
			if(staticValidCheck()) {
				options.push_back({pos, num});
			}
			grid[pos] = 0;
		}

		if(options.size() == 0) {
			return;
		}

		if(leastOptions.size() == 0 || options.size() <= leastOptions.size()) {
			leastOptions = options;
		}
	}


	if(leastOptions.size() == 0) {
		cout << "done" << endl;
		display();
		exit(0);

	}


	for(auto[pos, num] : leastOptions) {
		grid[pos] = num;
		solve();
		grid[pos] = 0;
	}
}




void initializePrimes() {
	for(int i = 2; i < 70; ++i) {
		prime[i] = true;
	}

	for(int i = 2; i < 70; ++i) {
		if(!prime[i]) {
			continue;
		}
		for(int j = i * i; j < 70; j += i) {
			prime[j] = false;
		}
	}
}


int main() {

	initializePrimes();

	int n, a, b, x, y;
	
	for(int i = 0; i < 4; ++i) {
		cin >> n;
		for(int j = 0; j < n; ++j) {
			cin >> a >> b >> x >> y;
			int pos0 = toPosition(a, b);
			int pos1 = toPosition(x, y);
			lines[i].push_back({pos0, pos1});
		}
	}

	for(int i = 0; i < 2; ++i) {
		for(int j = 0; j < 3; ++j) {
			int block = 2 * j + i;
			for(int x = 0; x < 3; ++x) {
				for(int y = 0; y < 2; ++y) {
					int subNum = 3 * y + x;
					int pos = toPosition(i * 3 + x, j * 2 + y);
					groups[block][subNum] = pos;
					blockNum[pos] = block;
				}
			}
		}
	}

	for(int x = 0; x < 6; ++x) {
		for(int y = 0; y < 6; ++y) {
			int pos = toPosition(x, y);
			groups[6 + x][y] = pos;
			groups[12 + y][x] = pos;
		}
	}


	for(int i = 0; i < POSITIONS; ++i) {
		grid[i] = 0;
	}

	solve();
}