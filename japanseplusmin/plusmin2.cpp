#include <bits/stdc++.h>

using namespace std;



/*

wauw ik word echt luier en luier.
Code is redelijk simpel deze keer. Prijs die ik daarvoor betaald heb: mijn laptop deed er 2,5 uur over.
Kan sowieso beter, maar doordat ik een progress counter had toegevoegd kon ik zien dat het binnen een overzichtelijke tijd (=minder dan een dag) klaar zou zijn.
Toen vond ik het wel prima om te wachten.


*/

typedef pair<int, int> ii;

#define MAXN 6

int n;

int toNode(int x, int y) {
    return x * n + y;
}


int values[MAXN * MAXN];


bool actualValue(int node) {
    return values[node] != 99 && values[node] != 0;
}

bool determined(int node) {
    return values[node] != 99;
}

stack<int> actions;


void doAction(ii action) {
    auto[node, num] = action;
    if(values[node] != 99) {
        cerr << "Oh oh" << endl;
        exit(0);
    }

    values[node] = num;
    actions.push(node);
}

void undo() {
    int node = actions.top();
    actions.pop();

    values[node] = 99;
}

vector<int> adjacent(int node) {
    vector<int> result;
    if((node % n) > 0) {
        result.push_back(node - 1);
    }
    if((node % n) < n - 1) {
        result.push_back(node + 1);
    }
    if(node >= n) {
        result.push_back(node - n);
    }
    if(node < n * n - n) {
        result.push_back(node + n);
    }

    return result;
}

vector<ii> allActions(int node) {
    vector<ii> result;
    for(int i = -n; i <= n; ++i) {
        result.push_back({node, i});
    }
    return result;
}

vector<ii> numberActions(int node) {
    vector<ii> result;
    for(int i = -n; i <= n; ++i) {
        if(i != 0) {
            result.push_back({node, i});
        }
    }
    return result;
}

void display() {
    for(int y = n - 1; y >= 0; --y) {
        for(int x = 0; x < n; ++x) {
            int num = values[toNode(x, y)];
            if(num == 99) {
                cout << "   ";
            } else if(num == 0) {
                cout << " x ";
            } else if(num > 0) {
                cout << " " << num << " ";
            } else {
                cout << num << " ";
            }
        }
        cout << endl;
    }
    
}

int hints[4 * MAXN];
int hintNodes[4 * MAXN][MAXN];
int hintAmount;


int hintFirst(int hint) {
    int i = 0;
    for(; i < n; ++i) {
        if(actualValue(hintNodes[hint][i])) {
            break;
        }
    }
    return i;
}

int hintValue(int hint) {
    int sum = 0;
    for(int i = hintFirst(hint); i < n; ++i) {
        if(!actualValue(hintNodes[hint][i])) {
            break;
        } else {
            sum += values[hintNodes[hint][i]];
        }
    }

    return sum;
}

bool checkersValid() {
    for(int i = 0; i < n * n; ++i) {
        if(!actualValue(i)) {
            continue;
        }
        for(int j : adjacent(i)) {
            if(!actualValue(j)) {
                continue;
            }

            if((values[i] > 0 && values[j] > 0) || (values[i] < 0 && values[j] < 0)) {
                return false;
            }
        }
    }
    return true;
}

bool uniqueValid() {
    bool seen[n];
    for(int hint = 0; hint < hintAmount; ++hint) {
        for(int i = 0; i < n; ++i) {
            seen[i] = false;
        }

        for(int i : hintNodes[hint]) {
            if(!actualValue(i)) {
                continue;
            }
            int num = abs(values[i]) - 1;
            if(seen[num]) {
                return false;
            }
            seen[num] = true;
        }
    }

    return true;
}

pair<int, vector<ii>> leastOptions(int depth) {
    const vector<ii> EMPTY;


    if(!checkersValid()) {
        return {0, EMPTY};
    }

    if(!uniqueValid()) {
        return {0, EMPTY};
    }


    if(depth == 0) {
        return {1, EMPTY};
    }

    vector<vector<ii>> optionLists;

    for(int hint = 0; hint < hintAmount; ++hint) {

        int value = hints[hint];

        if(value < 99 && value != hintValue(hint)) {
            vector<ii> options;

            int first = hintFirst(hint);

            for(int i = 0; i < first; ++i) {
                if(!determined(hintNodes[hint][i])) {
                    for(ii action : numberActions(hintNodes[hint][i])) {
                        options.push_back(action);
                    }
                }
            }

            int last = first;
            for(; last < n; ++last) {
                if(!actualValue(hintNodes[hint][last])) {
                    break;
                }
            }

            if(last < n && !determined(hintNodes[hint][last])) {
                for(ii action : allActions(hintNodes[hint][last])) {
                    options.push_back(action);
                }
            }

            optionLists.push_back(options);
        }
    }

    for(int i = n * n - 1; i >= 0; --i) {
        if(!determined(i)) {
            optionLists.push_back(allActions(i));
        }
    }

    if(optionLists.size() == 0) {
        cout << "done" << endl;
        display();
        exit(0);
    }


    pair<int, vector<ii>> best = {999999, EMPTY};

    for(vector<ii> optionList : optionLists) {
        int total = 0;
        int amount;
        vector<ii> actual;
        for(ii option : optionList) {
            doAction(option);
            amount = leastOptions(depth - 1).first;
            undo();

            if(amount > 0) {
                actual.push_back(option);
                total += amount;
            }
        }

        if(total < best.first) {
            if(total == 0) {
                return {0, EMPTY};
            }
            best = {total, actual};
        }
    }


    return best;



}


int iterations = 0;

void search(float start, float end) {


    
    auto[amount, options] = leastOptions(1);

    

    ++iterations;

    if(iterations % 50 == 0) {
        cerr << start << "%" << endl;
    }

    if(amount == 0) {
        return;
    }

    if(iterations % 1000 == 0) {
        display();
        cerr << iterations << " iterations" << endl;
        cerr << "amount " << amount << endl;
        cerr << hintValue(0) << endl;
        cerr << hintValue(4) << endl;
    }


    int optionAmount = options.size();

    for(int i = 0; i < optionAmount; ++i) {
        doAction(options[i]);
        search(start + (end - start) * i / optionAmount, start + (end - start) * (i + 1) / optionAmount);
        undo();
    }
}



int main() {


    cin >> n;

    for(int i = 0; i < n * n; ++i) {
        values[i] = 99;
    }

    hintAmount = 0;

    for(int k = 0; k < 4; ++k) {
        for(int i = 0; i < n; ++i) {
            cin >> hints[hintAmount];
            if(hints[hintAmount] == 99) {
                continue;
            }

            for(int j = 0; j < n; ++j) {
                if(k == 0) {
                    hintNodes[hintAmount][j] = toNode(i, j);
                } else if(k == 1) {
                    hintNodes[hintAmount][j] = toNode(i, n - 1 - j);
                } else if(k == 2) {
                    hintNodes[hintAmount][j] = toNode(j, i);
                } else {
                    hintNodes[hintAmount][j] = toNode(n - 1 - j, i);
                }
            }


            ++hintAmount;

        }
    }


    search(0, 100);


    return 0;
}