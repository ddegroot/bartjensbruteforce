#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;

#define MAXN 6

int n;

int toNode(int x, int y) {
    return x * n + y;
}


int values[MAXN * MAXN];

stack<int> actions;


void doAction(ii action) {
    auto[node, num] = action;
    if(values[node] != 0) {
        cerr << "Oh oh" << endl;
        exit(0);
    }

    values[node] = num;
    actions.push(node);
}

void undo() {
    int node = actions.top();
    actions.pop();

    values[node] = 0;
}

vector<int> adjacent(int node) {
    vector<int> result;
    if((node % n) > 0) {
        result.push_back(node - 1);
    }
    if((node % n) < n - 1) {
        result.push_back(node + 1);
    }
    if(node >= n) {
        result.push_back(node - n);
    }
    if(node < n * n - n) {
        result.push_back(node + n);
    }

    return result;
}

vector<ii> allActions(int node) {
    vector<ii> result;
    for(int i = 1; i <= 6; ++i) {
        result.push_back({node, i});
        result.push_back({node, -i});
    }
    return result;
}

void display() {
    for(int y = n - 1; y >= 0; --y) {
        for(int x = 0; x < n; ++x) {
            int num = values[toNode(x, y)];
            if(num >= 0) {
                cout << " " << num << " ";
            } else {
                cout << num << " ";
            }
        }
        cout << endl;
    }
    
}

int hintLeft[2 * MAXN];
int hintRight[2 * MAXN];
int hintNodes[2 * MAXN][MAXN];


int firstLeft(int hint) {
    int i = 0;
    for(; i < n; ++i) {
        if(values[hintNodes[hint][i]] != 0) {
            break;
        }
    }
    return i;
}

int firstRight(int hint) {
    int i = n - 1;
    for(; i >= 0; --i) {
        if(values[hintNodes[hint][i]] != 0) {
            break;
        }
    }
    return i;
}

int leftValue(int hint) {
    int sum = 0;
    for(int i = firstLeft(hint); i < n; ++i) {
        int num = values[hintNodes[hint][i]];
        if(num == 0) {
            break;
        } else {
            sum += num;
        }
    }

    return sum;
}

int rightValue(int hint) {
    int sum = 0;
    for(int i = firstRight(hint); i >= 0; --i) {
        int num = values[hintNodes[hint][i]];
        if(num == 0) {
            break;
        } else {
            sum += num;
        }
    }

    return sum;
}

bool connected(int hint) {
    int i = firstLeft(hint);
    for(; i < n; ++i) {
        if(values[hintNodes[hint][i]] == 0) {
            break;
        }
    }

    return i > firstRight(hint);
}

bool checkersValid() {
    for(int i = 0; i < n * n; ++i) {
        for(int j : adjacent(i)) {
            if((values[i] > 0 && values[j] > 0) || (values[i] < 0) && (values[j] < 0)) {
                return false;
            }
        }
    }
    return true;
}

bool uniqueValid() {
    bool seen[n];
    for(int hint = 0; hint < 2 * n; ++hint) {
        for(int i = 0; i < n; ++i) {
            seen[i] = false;
        }

        for(int i : hintNodes[hint]) {
            int num = abs(values[i]) - 1;
            if(num < 0) {
                continue;
            }
            if(seen[num]) {
                return false;
            }
            seen[num] = true;
        }
    }

    return true;
}

pair<int, vector<ii>> leastOptions(int depth) {
    const vector<ii> EMPTY;


    if(!checkersValid()) {
        return {0, EMPTY};
    }

    if(!uniqueValid()) {
        return {0, EMPTY};
    }


    if(depth == 0) {
        return {1, EMPTY};
    }

    vector<vector<ii>> optionLists;

    for(int hint = 0; hint < 2 * n; ++hint) {
        int left = hintLeft[hint];
        int right = hintRight[hint];
        if(left < 99 && right < 99 && left != right && connected(hint)) {
            vector<ii> options;


            for(int i = 0; i < n; ++i) {
                if(i >= firstLeft(hint) && i <= firstRight(hint)) {
                    continue;
                }
                for(ii action : allActions(hintNodes[hint][i])) {
                    options.push_back(action);
                }
            }

            //cerr << options.size() << endl;

            optionLists.push_back(options);
        }



        if(left < 99 && left != leftValue(hint)) {
            vector<ii> options;

            for(int i = 0; i < firstLeft(hint); ++i) {
                for(ii action : allActions(hintNodes[hint][i])) {
                    options.push_back(action);
                }
            }

            int end = firstLeft(hint);
            for(; end < n; ++end) {
                if(values[hintNodes[hint][end]] == 0) {
                    break;
                }
            }

            if(end < n) {
                for(ii action : allActions(hintNodes[hint][end])) {
                    options.push_back(action);
                }
            }

            optionLists.push_back(options);
        }

        if(right < 99 && right != rightValue(hint)) {
            vector<ii> options;

            for(int i = firstRight(hint) + 1; i < n; ++i) {
                for(ii action : allActions(hintNodes[hint][i])) {
                    options.push_back(action);
                }
            }

            int start = firstRight(hint);
            for(; start >= 0; --start) {
                if(values[hintNodes[hint][start]] == 0) {
                    break;
                }
            }

            if(start >= 0) {
                for(ii action : allActions(hintNodes[hint][start])) {
                    options.push_back(action);
                }
            }

            optionLists.push_back(options);
        }
    }


    pair<int, vector<ii>> best = {999999, EMPTY};

    for(vector<ii> optionList : optionLists) {
        int total = 0;
        int amount;
        vector<ii> actual;
        for(ii option : optionList) {
            doAction(option);
            amount = leastOptions(depth - 1).first;
            undo();

            if(amount > 0) {
                actual.push_back(option);
                total += amount;
            }
        }

        if(total < best.first) {
            if(total == 0) {
                return {0, EMPTY};
            }
            best = {total, actual};
        }
    }


    return best;



}


int iterations = 0;

void search(float start, float end) {


    
    auto[amount, options] = leastOptions(1);

    

    ++iterations;

    if(iterations % 50 == 0) {
        cerr << start << "%" << endl;
    }

    if(amount == 0) {
        return;
    }

    if(iterations % 1000 == 0) {
        display();
        cerr << iterations << " iterations" << endl;
        cerr << "amount " << amount << endl;
    }


    int optionAmount = options.size();

    for(int i = 0; i < optionAmount; ++i) {
        doAction(options[i]);
        search(start + (end - start) * i / optionAmount, start + (end - start) * (i + 1) / optionAmount);
        undo();
    }
}



int main() {

    cin >> n;

    for(int k = 0; k < 2; ++k) {
        int num;
        for(int i = 0; i < n; ++i) {
            cin >> num;
            hintLeft[k * n + i] = num;
        }
        for(int i = 0; i < n; ++i) {
            cin >> num;
            hintRight[k * n + i] = num;
        }


        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                if(k == 0) {
                    hintNodes[k * n + i][j] = toNode(i, j);
                } else {
                    hintNodes[k * n + i][j] = toNode(j, i);
                }
            }
        }
    }


    search(0, 100);


    return 0;
}