#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;

int group[9][9];

int hasBee[9][9];
int rowBees[9];
int columnBees[9];
int groupBees[9];
int bees = 0;

int hasBird[9][9];
int rowBirds[9];
int columnBirds[9];
int groupBirds[9];
int birds = 0;


ii next(ii pos) {
	if(pos.first == 8) {
		return {0, pos.second + 1};
	}
	return {pos.first + 1, pos.second};
}

bool validPos(ii pos) {
	return pos.second < 9;
}

void addBee(int x, int y, int amount) {
	rowBees[x] += amount;
	columnBees[y] += amount;
	groupBees[group[x][y]] += amount;
	bees += amount;
	hasBee[x][y] += amount;
}

void addBird(int x, int y, int amount) {
	rowBirds[x] += amount;
	columnBirds[y] += amount;
	groupBirds[group[x][y]] += amount;
	birds += amount;
	hasBird[x][y] += amount;
}

int containsBee(int x, int y) {
	if(x < 0 || x >= 9) {
		return false;
	}
	if(y < 0 || y >= 9) {
		return false;
	}
	return hasBee[x][y];
}

int containsBird(int x, int y) {
	if(x < 0 || x >= 9) {
		return false;
	}
	if(y < 0 || y >= 9) {
		return false;
	}
	return hasBird[x][y];
}

bool canPlaceBee(int x, int y) {
	for(int xOffset = -1; xOffset <= 1; ++xOffset) {
		for(int yOffset = -1; yOffset <= 1; ++yOffset) {
			if(containsBee(x + xOffset, y + yOffset)) {
				return false;
			}
		}
	}

	if(rowBees[x] > 1) {
		return false;
	}
	if(columnBees[y] > 1) {
		return false;
	}
	if(groupBees[group[x][y]] > 1) {
		return false;
	}

	return true;
}

bool canPlaceBird(int x, int y) {

	for(int xOffset = -1; xOffset <= 1; ++xOffset) {
		for(int yOffset = -1; yOffset <= 1; ++yOffset) {
			if(containsBird(x + xOffset, y + yOffset)) {
				return false;
			}
		}
	}

	int adjacentBees = 0;


	for(int xOffset = -1; xOffset <= 1; ++xOffset) {
		for(int yOffset = -1; yOffset <= 1; ++yOffset) {
			if(xOffset * xOffset + yOffset * yOffset != 1) {
				continue;
			}

			if(x + xOffset < 0 || x + xOffset >= 9) {
				continue;
			}

			if(y + yOffset < 0 || y + yOffset >= 9) {
				continue;
			}

			if(group[x][y] != group[x + xOffset][y + yOffset]) {
				continue;
			}

			if(containsBee(x + xOffset, y + yOffset)) {
				++adjacentBees;
			}
		}
	}

	if(adjacentBees == 0) {
		return false;
	}

	if(rowBirds[x] > 0) {
		return false;
	}
	if(columnBirds[y] > 0) {
		return false;
	}
	if(groupBirds[group[x][y]] > 0) {
		return false;
	}

	return true;
}

long long counter = 0;

void output() {
	for(int i = 0; i < 9; ++i) {
		for(int j = 0; j < 9; ++j) {
			if(hasBee[i][j]) {
				cout << "B";
			} else if(hasBird[i][j]) {
				cout << "V";
			} else {
				cout << "-";
			}
		}
		cout << endl;
	}
}



long long goBirds(ii lastPos) {
	++counter;


	if(birds == 9) {
		output();
		return 1;
	}

	ii nextPos = next(lastPos);

	if(lastPos.first == 8) {
		if(columnBirds[lastPos.second] != 1) {
			return 0;
		}
	}
	

	if(!validPos(nextPos)) {
		return 0;
	}

	long long amount = 0;

	if(canPlaceBird(nextPos.first, nextPos.second)) {
		addBird(nextPos.first, nextPos.second, 1);
		amount += goBirds(nextPos);
		addBird(nextPos.first, nextPos.second, -1);

	}

	amount += goBirds(nextPos);

	return amount;
}


long long go(ii lastPos) {
	++counter;


	if(bees == 18) {
		goBirds({-1, 0});
		return 1;
	}

	ii nextPos = next(lastPos);

	if(lastPos.first == 8) {
		if(columnBees[lastPos.second] != 2) {
			return 0;
		}
	}
	

	if(!validPos(nextPos)) {
		return 0;
	}

	long long amount = 0;

	if(canPlaceBee(nextPos.first, nextPos.second)) {
		addBee(nextPos.first, nextPos.second, 1);
		amount += go(nextPos);
		addBee(nextPos.first, nextPos.second, -1);

	}

	amount += go(nextPos);

	return amount;

}


int main() {
	string s;
	for(int i = 0; i < 9; ++i) {
		cin >> s;
		for(int j = 0; j < 9; ++j) {
			group[i][j] = s[j] - '1';
		}
	}

	int solutions = go({-1, 0});

	cout << "Solutions: " << solutions << endl;


	cout << "Iterations: " << counter << endl;

}